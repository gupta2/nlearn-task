const MySql = require("../config/db_config");

module.exports = {
    getAllNewAssignments(req,res){
        //query for newly added assignments (if the all students progress_status is not submitted then it is return as new assignment )
        let get_all_new_assignments_query = "select * from assignments where assignment_id in (select assignment_id from assignment_courses_mapping where id in (select assignment_courses_mapping_id from progress_on_assignments where progress_status_id=(select id from progress_status where progress_description='Not submitted'))) order by created_date desc"
        MySql.sqlQuery(get_all_new_assignments_query,(err,result)=>{
            if(err)
            {
                res.send({"status":fasle,"message":err.sqlMessage});
            }
            else
            {
                res.send({"status":true,"data":result,"message":"Successfully retrieved assignments"});
            }
        })
    },
    getStudentCourseInfo(req,res){
        //query to get student name and course name
        let query = "select student_name,course_name from students as s inner join student_coures_registrations as scr on s.student_id = scr.student_id inner join courses_scheduled cs on cs.id = scr.courses_schduled_id inner join courses_offered   co on co.course_id = cs.course_id"
        MySql.sqlQuery(query,(err,result)=>{
            if(err)
            {
                res.send({"status":fasle,"message":err.sqlMessage});
            }
            else
            {
                object = {}
                for(each in result)
                {
                    //checking student name exists in object
                    if(object.hasOwnProperty(result[each]["student_name"]))
                    {
                        //to remove duplicates
                        if(object[result[each]["student_name"]].indexOf(result[each]["course_name"])==-1)
                            object[result[each]["student_name"]].push(result[each]["course_name"])
                    }
                    else{
                        object[result[each]["student_name"]] = [result[each]["course_name"]]
                    }
                }
                res.send({"status":true,"message":"Student details with course name","data":object})
            }

        });
        // another method
        //  let data = []
        // MySql.sqlQuery(student_query,(err,result)=>{
        //     if(err)
        //     {
        //         res.send({"status":fasle,"message":err.sqlMessage});
        //     }
        //     else
        //     {
        //         function eachStudentCourseDetails(index,result,callback)
        //         {
        //             let student_id = result[index]["student_id"];
        //             let course_details_result = "select course_name from courses_offered where course_id in (select course_id from courses_scheduled where id in (select courses_schduled_id from student_coures_registrations where student_id = "+student_id+" ))"
        //             MySql.sqlQuery(course_details_result,(err,course_result)=>{
        //                 details = {"student_name":result[index],"course_names":[]}
        //                 if(err)
        //                 {
        //                     if(result.length-1>index)
        //                     {
        //                         eachStudentCourseDetails(index+1,result,callback)
        //                     }
        //                     else{
        //                         callback()
        //                     }
        //                 }
        //                 else
        //                 {
                            
        //                     for(i in course_result)
        //                     {
        //                         details["course_names"].push(course_result[i]["course_name"]);
        //                     }
        //                     data.push(details)
        //                     if(result.length-1>index)
        //                     {
        //                         eachStudentCourseDetails(index+1,result,callback)
        //                     }
        //                     else{
        //                         callback()
        //                     }
        //                 }
        //             });
        //         }
        //         eachStudentCourseDetails(0,result,()=>{
        //             res.send({"status":true,"message":"Student details with course name","data":data})
        //         })
        //     }
        // })
    },
    getStudentNamesForEachCourse(req,res){
        
        let query = "select student_name,course_name from students as s inner join student_coures_registrations as scr on s.student_id = scr.student_id inner join courses_scheduled cs on cs.id = scr.courses_schduled_id inner join courses_offered   co on co.course_id = cs.course_id"
        MySql.sqlQuery(query,(err,result)=>{
            if(err)
            {
                res.send({"status":fasle,"message":err.sqlMessage});
            }
            else
            {
                object = {}
                for(each in result)
                {
                    if(object.hasOwnProperty(result[each]["course_name"]))
                    {
                        if(object[result[each]["course_name"]].indexOf(result[each]["student_name"])==-1)
                            object[result[each]["course_name"]].push(result[each]["student_name"])
                    }
                    else{
                        object[result[each]["course_name"]] = [result[each]["student_name"]]
                    }
                }
                res.send({"status":true,"message":"Student details with course name","data":object})
            }

        });
        // another method
        // let courses_query = "select * from courses_offered"
        // let data = []
        // MySql.sqlQuery(courses_query,(err,result)=>{
        //     if(err)
        //     {
        //         res.send({"status":fasle,"message":err.sqlMessage});
        //     }
        //     else
        //     {
                
        //         // function eachStudentCourseDetails(index,result,callback)
        //         // {
        //         //     let course_id = result[index]["course_id"];
        //         //     let course_details_result = "select student_name from students where student_id in (select student_id from student_coures_registrations where courses_schduled_id in (select id from courses_scheduled where course_id = "+course_id+" ))"
        //         //     MySql.sqlQuery(course_details_result,(err,course_result)=>{
        //         //         details = {"course_name":result[index],"students":[]}
        //         //         if(err)
        //         //         {
        //         //             if(result.length-1>index)
        //         //             {
        //         //                 eachStudentCourseDetails(index+1,result,callback)
        //         //             }
        //         //             else{
        //         //                 callback()
        //         //             }
        //         //         }
        //         //         else
        //         //         {
                            
        //         //             for(i in course_result)
        //         //             {
        //         //                 details["students"].push(course_result[i]["student_name"]);
        //         //             }
        //         //             data.push(details)
        //         //             if(result.length-1>index)
        //         //             {
        //         //                 eachStudentCourseDetails(index+1,result,callback)
        //         //             }
        //         //             else{
        //         //                 callback()
        //         //             }
        //         //         }
        //         //     });
        //         // }
        //         // eachStudentCourseDetails(0,result,()=>{
        //         //     res.send({"status":true,"message":"Student details with course name","data":data})
        //         // })
        //     }
        // });
    }
};