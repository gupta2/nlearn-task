let prodConfig = {
    "server": {
        "host": "0.0.0.0",
        "port": "1366"
    },
    "sql":{
        "multipleStatements": 'true',
        "host" : process.env.sqlHost ? process.env.sqlHost : 'localhost',
        "user" : process.env.sqlUser ? process.env.sqlUser : 'root',
        "password" : process.env.sqlPassword ? process.env.sqlPassword : '\''
    },
}
module.exports = prodConfig;