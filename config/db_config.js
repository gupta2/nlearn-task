const mysql = require("mysql");
const config = require('./server_config');
let pool =  mysql.createPool({
    connectionLimit : 20,
    host     : config.sql.host,
    user     : config.sql.user,
    password : config.sql.password,
    database : 'nLearn',
    debug    :  false,
    multipleStatements: true
});
function sqlQuery(query, callback)
{
    pool.getConnection(function(err, connection) {
    if (err) throw err; // not connected!
    //console.log("------------");
    // Use the connection
    connection.query(query, function (error, results, fields) {
        //console.log("------------",results);
            // When done with the connection, release it.
            connection.release();
        
            // Handle error after the release.
            
            if (error) 
            {
            //console.log(query);
            throw error;
            }
            //console.log("------------",results);
            callback(error,results)
            // Don't use the connection here, it has been returned to the pool.
        });
    });
}
module.exports = {
    sqlQuery : sqlQuery
  };