const express = require("express");
const bodyParser = require("body-parser");
const config = require("./config/server_config")
const app = express();
var cors = require('cors')
const students = require('./routes/students.routes');
process.on('uncaughtException', function (err) {
    console.error(err);
    console.log("Node NOT Exiting...");
});

const options = {
    allowedHeaders: ["Origin","Authorization","X-Requested-With","Content-Type", "Accept", "X-Access-Token","X-Forwarded-For"],
    credentials: true,
    methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
    origin: "*",
    preflightContinue: false
};

app.use(cors(options));
app.use('/api',students)
app.options("*", cors(options));

let server =app.listen(config.server.port, err => {
    if (err) {
        console.error(err);
    } else {
        console.log(`App listening on port: 1366`);
    }
});