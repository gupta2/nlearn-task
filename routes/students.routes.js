const  Router  = require('express');
const  StudentController = require('../controllers/students.controller')
const router = new Router();

//routes
router.route('/assignments').get(StudentController.getAllNewAssignments);
router.route('/student-with-courses').get(StudentController.getStudentCourseInfo);
router.route('/course-with-students').get(StudentController.getStudentNamesForEachCourse)

module.exports = router;